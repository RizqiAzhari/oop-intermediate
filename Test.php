<?php

// require "Kelas/Elang.php";
// require "Kelas/Harimau.php";

// spl_autoload_register(function ($class) {
//     $class = explode('\\', $class);
//     $class = end($class);
//     require __DIR__ . '/Kelas/' . $class . '.php';
// });

use Kelas\Elang;
use Kelas\Harimau;

$elang_1 = new Elang('elang_1', 2, "terbang tinggi", 10, 5);
$harimau_1 = new Harimau('harimau_1', 4, "lari cepat", 7, 8);

$elang_1->atraksi();
$elang_1->serang($harimau_1->nama);
$elang_1->diserang($harimau_1->defencePower);
$elang_1->getInfoHewan();

$harimau_1->atraksi();
$harimau_1->serang($elang_1->nama);
$harimau_1->diserang($elang_1->defencePower);
$harimau_1->getInfoHewan();

?>