<?php

require '../Model/Hewan.php';
require '../Model/Fight.php';

class Harimau {
    use Hewan;
    use Fight;

    public function serang($namaLawan) {
        echo $this->nama." sedang menyerang ".$namaLawan."<br><br>";
    }

    public function diserang($defencePower) {
        echo $this->nama." sedang diserang"."<br><br>";
        $power = $this->attackPower / $defencePower;
        $this->darah -= $power;
    }

    public function getInfoHewan(){
        echo 'Jenis : Harimau<br><br>';
        echo 'Nama : '.$this->nama.'<br>';
        echo 'Darah : '.$this->darah.'<br>';
        echo 'Jumlah Kaki : '.$this->jumlahKaki.'<br>';
        echo 'Keahlian : '.$this->keahlian.'<br>';
        echo 'Attack Power : '.$this->attackPower.'<br>';
        echo 'Defence Power : '.$this->defencePower.'<br>';
    }
}

?>