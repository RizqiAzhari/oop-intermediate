<?php

require '../Model/Hewan.php';
require '../Model/Fight.php';

class Elang {
    use Hewan;
    use Fight;

    public function serang($namaLawan) {
        echo $this->nama." sedang menyerang ".$namaLawan;
    }

    public function diserang($defencePower) {
        echo $this->nama." sedang diserang";
        $power = $this->attackPower / $defencePower;
        $this->darah -= $power;
    }

    public function getInfoHewan(){
        echo 'Jenis : Elang<br><br>';
        echo 'Nama : '.$this->nama.'<br>';
        echo 'Darah : '.$this->darah.'<br>';
        echo 'Jumlah Kaki : '.$this->jumlahKaki.'<br>';
        echo 'Keahlian : '.$this->keahlian.'<br>';
        echo 'Attack Power : '.$this->attackPower.'<br>';
        echo 'Defence Power : '.$this->defencePower.'<br>';
    }
}

?>